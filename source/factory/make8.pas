
uses
  SysUtils, Board;
  
var
  t: text;
  x, y: integer;
  LBoard: TBoard;
  
begin  
  Assign(t, 'whitesquares.pas');
  Rewrite(t);
  
  LBoard := 0;
  for x := 0 to 7 do
    for y := 0 to 7 do
      if (x + y) mod 2 = 1 then
        SwitchOn(LBoard, ToBoard(x, y));
  
  Write(t, ' $', IntToHex(LBoard, 16));
  Close(t);
  
  PrintBoard(LBoard);
end.

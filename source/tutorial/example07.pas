
uses
  SysUtils, Board, Bitboards, Chess, Moves, Castling;

const
  CPos: array[0..3] of string = (
    'rnbqkbnr/pp1ppppp/2p5/8/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -',
    'rnbqkbnr/2pppppp/p7/Pp6/8/8/1PPPPPPP/RNBQKBNR w KQkq b6',
    'rnbqkbnr/ppp3pp/3p1p2/4p3/4P3/5N2/PPPPBPPP/RNBQK2R w KQkq -',
    'rnbqk1nr/2pp1ppp/pp2p3/1Bb5/4P3/5P1N/PPPP2PP/RNBQK2R w KQkq -'
  );

{ Génération des coups pour une position donnée. }

var
  LPos: TPosition;
  LMoves: array[0..199] of integer;
  LCount, i: integer;

begin
  LPos := EncodePosition(CPos[3]);
  
  WriteLn(PosToText(LPos));
  WriteLn;
  
  GenerateMoves(LPos, LMoves, LCount);
  GenerateCastling(LPos, LMoves, LCount);
  
  for i := 0 to Pred(LCount) do
    WriteLn(MoveToDebugStr(LMoves[i]));
end.

{
  +  A B C D E F G H  +
  8 |r|n|b|q|k|:|n|r| 8
  7 |:|.|p|p|:|p|p|p| 7
  6 |p|p|.|:|p|:|.|:| 6
  5 |:|B|b|.|:|.|:|.| 5
  4 |.|:|.|:|P|:|.|:| 4
  3 |:|.|:|.|:|P|:|N| 3
  2 |P|P|P|P|.|:|P|P| 2
  1 |R|N|B|Q|K|.|:|R| 1
  +  A B C D E F G H  + *
  Castling: HAha
  En passant: -
  FEN: rnbqk1nr/2pp1ppp/pp2p3/1Bb5/4P3/5P1N/PPPP2PP/RNBQK2R w HAha -
  
  b1a3-ptKnight-[]
  b1c3-ptKnight-[]
  d1e2-ptQueen-[]
  e1f1-ptKing-[]
  e1e2-ptKing-[]
  e1f2-ptKing-[]
  h1f1-ptRook-[]
  h1g1-ptRook-[]
  a2a3-ptWhitePawn-[]
  a2a4-ptWhitePawn-[]
  b2b3-ptWhitePawn-[]
  b2b4-ptWhitePawn-[]
  c2c3-ptWhitePawn-[]
  c2c4-ptWhitePawn-[]
  d2d3-ptWhitePawn-[]
  d2d4-ptWhitePawn-[]
  g2g3-ptWhitePawn-[]
  g2g4-ptWhitePawn-[]
  f3f4-ptWhitePawn-[]
  h3g1-ptKnight-[]
  h3f2-ptKnight-[]
  h3f4-ptKnight-[]
  h3g5-ptKnight-[]
  e4e5-ptWhitePawn-[]
  b5f1-ptBishop-[]
  b5e2-ptBishop-[]
  b5d3-ptBishop-[]
  b5a4-ptBishop-[]
  b5c4-ptBishop-[]
  b5a6-ptBishop-[mtCapture]
  b5c6-ptBishop-[]
  b5d7-ptBishop-[mtCapture]
}

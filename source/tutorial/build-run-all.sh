
rm -f *.out
rm -f units/*.*

for f in example??.pas
do
  fpc -Mobjfpc -Sh -Fu.. -FUunits $f | tee fpc-${f%.pas}.out
  if [ -f ${f%.pas} ]
  then
    ./${f%.pas} > ${f%.pas}.out
  fi
done

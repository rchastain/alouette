
unit Settings;

interface

procedure LoadSettings(
  out AUseBook: boolean;
  out
  APiecePrior,
  APawnStruct,
  ATargets,
  ACastling,
  AEnPassant,
  ACheck,
  AProtect,
  AAttack,
  AMobility,
  ANewPos: integer
);
procedure SaveSettings(
  const AUseBook: boolean;
  const
  APiecePrior,
  APawnStruct,
  ATargets,
  ACastling,
  AEnPassant,
  ACheck,
  AProtect,
  AAttack,
  AMobility,
  ANewPos: integer
);
function SettingsFileExists: boolean;

implementation

uses
  SysUtils, IniFiles;

const
  CMain = 'main';
  CCoef = 'coefficients';
  
var
  LIniPath: string;

procedure LoadSettings(
  out AUseBook: boolean;
  out
  APiecePrior,
  APawnStruct,
  ATargets,
  ACastling,
  AEnPassant,
  ACheck,
  AProtect,
  AAttack,
  AMobility,
  ANewPos: integer
);
const
  CUseBook    =  1;
  CPiecePrior =  1;
  CPawnStruct =  1;
  CTargets    =  1;
  CCastling   = 10;
  CEnPassant  = 10;
  CCheck      =  1;
  CProtect    =  1;
  CAttack     =  0;
  CPawnMobil  =  0;
  CNewPos     =  5;
begin
  with TIniFile.Create(LIniPath) do
  try
    AUseBook := boolean(ReadInteger(CMain, 'usebook',    CUseBook));
    APiecePrior      := ReadInteger(CCoef, 'pieceprior', CPiecePrior);
    APawnStruct      := ReadInteger(CCoef, 'pawnstruct', CPawnStruct);
    ATargets         := ReadInteger(CCoef, 'targets',    CTargets);
    ACastling        := ReadInteger(CCoef, 'castling',   CCastling);
    AEnPassant       := ReadInteger(CCoef, 'enpassant',  CEnPassant);
    ACheck           := ReadInteger(CCoef, 'check',      CCheck);
    AProtect         := ReadInteger(CCoef, 'protect',    CProtect);
    AAttack          := ReadInteger(CCoef, 'attack',     CAttack);
    AMobility        := ReadInteger(CCoef, 'pawnmobil',  CPawnMobil);
    ANewPos          := ReadInteger(CCoef, 'newpos',     CNewPos);
  finally
    Free;
  end;
end;

procedure SaveSettings(
  const AUseBook: boolean;
  const
  APiecePrior,
  APawnStruct,
  ATargets,
  ACastling,
  AEnPassant,
  ACheck,
  AProtect,
  AAttack,
  AMobility,
  ANewPos: integer
);
begin
  with TIniFile.Create(LIniPath) do
  try
    WriteInteger(CMain, 'usebook',    Ord(AUseBook));
    WriteInteger(CCoef, 'pieceprior', APiecePrior);
    WriteInteger(CCoef, 'pawnstruct', APawnStruct);
    WriteInteger(CCoef, 'targets',    ATargets);
    WriteInteger(CCoef, 'castling',   ACastling);
    WriteInteger(CCoef, 'enpassant',  AEnPassant);
    WriteInteger(CCoef, 'check',      ACheck);
    WriteInteger(CCoef, 'protect',    AProtect);
    WriteInteger(CCoef, 'attack',     AAttack);
    WriteInteger(CCoef, 'pawnmobil',  AMobility);
    WriteInteger(CCoef, 'newpos',     ANewPos);
    UpdateFile;
  finally
    Free;
  end;
end;

function SettingsFileExists: boolean;
begin
  result := FileExists(LIniPath);
end;

begin
  LIniPath := ChangeFileExt(ParamStr(0), '.cfg');
end.
